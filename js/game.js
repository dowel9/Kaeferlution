var globalID;
const canvas = document.querySelector("canvas");
var context = canvas.getContext("2d");
var currentRound = -1;
var startTime = null;
var roundRunTime = 0;
var bugList = [];
var freezeGame = false;
var drawHitBox = false;
var points = 0;
var timeDiff = 16;
var statsPageSelectionInHtml = 0;

function main(timestamp) {
    globalID = requestAnimationFrame(main);

    // Clear Canvas
    context.clearRect(0, 0, canvas.width, canvas.height);

    // Time
    let previousRoundTime = roundRunTime;
    if (!startTime) startTime = timestamp;
    roundRunTime = timestamp - startTime;
    timeDiff = roundRunTime - previousRoundTime;
    if (isNaN(timeDiff)) timeDiff = 16;
    let remainingTime = settings.general_time * 1000 - roundRunTime;
    remainingTime = Math.max(0, remainingTime);
    let time = tools.msToHMS(remainingTime);
    context.font = "48px serif";
    context.strokeText(time.hours + ":" + time.minutes + ":" + time.seconds, canvas.width / 2.3, 50);

    // Check if the round is over (Time ran out OR Points gathered)
    if ((settings.general_pointsNeeded <= points && settings.general_pointsNeeded > 0) || remainingTime <= 0) {
        roundOver();

        // Return to stop the bugs moving around after the round
        cancelAnimationFrame(globalID);
        return;
    }

    // Draw and move all bugs
    moveDrawBugs();
}

function startNewRound() {
    currentRound++;
    points = 0;
    statsPageSelectionInHtml = currentRound;
    updateSettings();
    main();
    repopulate();
}

function roundOver() {
    // Save all remaining bugs
    // If pushing just bugList we only push a reference so we need to make an actual copy
    let copyOfBugList = [...bugList];
    let roundStats = new RoundStats(currentRound, copyOfBugList);
    statsCollector.push(roundStats);

    // New screen with graphs and two buttons "continue" and "return to main menu"
    document.getElementById("gameWrapper").style.display = "none";
    document.getElementById("statsWrapper").style.display = "block";

    //Show the plots for this round
    showPlots(currentRound);
}

function showPlots(round) {
    if (round < 0 || round > currentRound) return;

    // Create the plots
    let dataSpeed = statsCollector[round].formatDataSpeed();
    Plotly.newPlot(dataSpeed.html, dataSpeed.data, dataSpeed.layout);
    let dataSize = statsCollector[round].formatDataSize();
    Plotly.newPlot(dataSize.html, dataSize.data, dataSize.layout);
    if (settings.rndMovement_allowed) {
        let dataRngMovement = statsCollector[round].formatDataRngMovement();
        Plotly.newPlot(dataRngMovement.html, dataRngMovement.data, dataRngMovement.layout);
    }
    if (settings.opacity_allowed) {
        let dataOpacity = statsCollector[round].formatDataOpacity();
        Plotly.newPlot(dataOpacity.html, dataOpacity.data, dataOpacity.layout);
    }
}

function showAveragePlots() {
    // TODO: DRY-Principle with showPlots-function...
    let x = [...Array(currentRound + 1).keys()];
    let ys = { speed: [], size: [], rngMovementCD: [], opacity: [] };
    for (let i = 0; i < currentRound + 1; i++) {
        let averages = statsCollector[i].getAverages();
        for (const [key, value] of Object.entries(ys)) {
            ys[key].push(averages[key]);
        }
    }
    let dataSpeed = basicDataLayout("Durchschnittliche Geschwindigkeit", "statsDivSpeed", "scatter", x, ys["speed"]);
    let dataSize = basicDataLayout("Durchschnittliche Größe", "statsDivSize", "scatter", x, ys["size"]);
    let dataRnGMov = basicDataLayout(
        "Durchschnittliche Zeit zum Richtungswechsel",
        "statsDivRnGMovement",
        "scatter",
        x,
        ys["rngMovementCD"]
    );
    let dataOpacity = basicDataLayout(
        "Durchschnittliche Durchsichtbarkeit",
        "statsDivOpacity",
        "scatter",
        x,
        ys["opacity"]
    );
    console.log(dataSpeed);
    Plotly.newPlot(dataSpeed.html, dataSpeed.data, dataSpeed.layout);
    Plotly.newPlot(dataSize.html, dataSize.data, dataSize.layout);
    if (settings.rndMovement_allowed) Plotly.newPlot(dataRnGMov.html, dataRnGMov.data, dataRnGMov.layout);
    if (settings.opacity_allowed) Plotly.newPlot(dataOpacity.html, dataOpacity.data, dataOpacity.layout);
}

function moveDrawBugs() {
    bugList.forEach((element) => {
        if (!freezeGame) element.move();
        element.draw();
        if (drawHitBox) element.drawHitBox();
    });
}

function repopulate() {
    updateSettings();
    let bugsNeeded = settings.general_bugAmount - bugList.length;
    //let bugsNeeded = 1;
    let restartFromNew = bugList.length == 0;

    for (let index = 0; index < bugsNeeded; index++) {
        var newBug = null;
        if (restartFromNew) newBug = new Kaefer();
        else {
            // We still have bugs remaining so we let them lay eggs
            let chosenBug = tools.randomNumber(0, bugList.length - 1);
            newBug = bugList[chosenBug].layEgg();
        }
        bugList.push(newBug);
    }
}

function clickOnBug(point) {
    let bugsClickedOn = [];
    bugList.forEach((element) => {
        let hit = element.checkHit(point);
        if (hit) {
            bugsClickedOn.push(element.ID);
            points++;
        }
    });

    // Remove the bugs we got from the game
    bugsClickedOn.forEach((removeID) => {
        var i = 0;
        while (i < bugList.length) {
            if (bugList[i].ID === removeID) {
                bugList.splice(i, 1);
            } else {
                ++i;
            }
        }
    });
}

document.addEventListener("DOMContentLoaded", function (event) {
    // Your code to run since DOM is loaded and ready
    // Properly scale the canvas to match coordinates and pixels
    tools.resizeCanvasToDisplaySize(canvas);
});

document.addEventListener("keydown", (event) => {
    if (
        document.getElementById("statsWrapper").style.display == "none" ||
        document.getElementById("statsWrapper").style.display == ""
    )
        return;
    if (event.keyCode == 39) {
        statsPageSelectionInHtml = Math.min(statsPageSelectionInHtml + 1, currentRound);
    }
    if (event.keyCode == 37) {
        statsPageSelectionInHtml = Math.max(statsPageSelectionInHtml - 1, 0);
    }
    showPlots(statsPageSelectionInHtml);
});

canvas.addEventListener("mousedown", function (e) {
    let point = tools.getCursorPosition(canvas, e);
    clickOnBug(point);
});

window.addEventListener(
    "resize",
    function (e) {
        context.imageSmoothingEnabled = false;
    },
    false
);
