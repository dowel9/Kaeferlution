class Point2D {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    add(otherPoint) {
        this.x += otherPoint.x;
        this.y += otherPoint.y;
    }

    sub(otherPoint) {
        this.x -= otherPoint.x;
        this.y -= otherPoint.y;
    }

    mult(scalar) {
        this.x *= scalar;
        this.y *= scalar;
    }

    div(scalar) {
        this.x /= scalar;
        this.y /= scalar;
    }

    distance(otherPoint) {
        let dist = Math.abs(this.x - otherPoint.x) + Math.abs(this.y - otherPoint.y);
        return dist;
    }
}

class Vector2D extends Point2D {
    constructor(x, y) {
        super(x, y);
    }

    getLength() {
        let len = Math.sqrt(this.x * this.x + this.y * this.y);
        return len;
    }

    normalize() {
        let len = this.getLength();
        this.x /= len;
        this.y /= len;
    }

    rotate(angle) {
        angle = (Math.PI * angle) / 180;
        let newX = Math.cos(angle) * this.x - Math.sin(angle) * this.y;
        let newY = Math.sin(angle) * this.x + Math.cos(angle) * this.y;
        this.x = newX;
        this.y = newY;
    }

    dotProduct(otherVector) {
        let dotProd = this.x * otherVector.x + this.y * otherVector.y;
        return dotProd;
    }

    determinant(otherVector) {
        let det = this.x * otherVector.y - this.y * otherVector.x;
        return det;
    }
}
