var tools = (function () {
    var canvas2D = document.getElementById("gameCanvas");
    var mousePos = new Point2D(0, 0);

    // function getRandomPositionInCanvas: Will return a random Point within the boundaries of the game-canvas.
    var getRandomPositionInCanvas = function (dist = 0) {
        let rndX = randomNumber(dist, canvas2D.width - dist);
        let rndY = randomNumber(dist, canvas2D.height - dist);
        let point = new Point2D(rndX, rndY);
        return point;
    };

    // function randomNumber: Expects min(float/int), max(float/int), hasToBeInteger(boolean). Generates random number between min and max. Returns rndNum(int/float)
    var randomNumber = function (min, max, hastoBeInteger = true) {
        let rndNum = Math.random() * (max - min + 1) + min;
        if (hastoBeInteger) rndNum = Math.floor(rndNum);

        return rndNum;
    };

    var cutOffNumbers = function (number, placesAfterCommaLeft) {
        number *= Math.pow(10, placesAfterCommaLeft);
        number = Math.floor(number);
        number /= Math.pow(10, placesAfterCommaLeft);

        return number;
    };

    var setCursorPosition = function (canvas, event) {
        const rect = canvas.getBoundingClientRect();
        mousePos.x = event.clientX - rect.left;
        mousePos.y = event.clientY - rect.top;
    };

    var getCursorPosition = function () {
        return mousePos;
    };

    var resizeCanvasToDisplaySize = function (canvas) {
        // look up the size the canvas is being displayed
        const width = canvas.clientWidth;
        const height = canvas.clientHeight;

        // If it's resolution does not match change it
        if (canvas.width !== width || canvas.height !== height) {
            canvas.width = width;
            canvas.height = height;
            return true;
        }

        return false;
    };

    var msToHMS = function (ms) {
        // https://stackoverflow.com/questions/29816872/how-can-i-convert-milliseconds-to-hhmmss-format-using-javascript
        // 1- Convert to seconds:
        let seconds = ms / 1000;
        // 2- Extract hours:
        var hours = parseInt(seconds / 3600); // 3,600 seconds in 1 hour
        hours = hours <= 9 ? "0" + hours : hours;
        seconds = seconds % 3600; // seconds remaining after extracting hours
        // 3- Extract minutes:
        var minutes = parseInt(seconds / 60); // 60 seconds in 1 minute
        minutes = minutes <= 9 ? "0" + minutes : minutes;
        // 4- Keep only seconds not extracted to minutes:
        seconds = seconds % 60;
        seconds = Math.floor(Math.floor(1000 * seconds) / 1000);
        seconds = seconds <= 9 ? "0" + seconds : seconds;
        return { hours: hours, minutes: minutes, seconds: seconds };
    };

    var deg2rad = function (d) {
        return (d * Math.PI) / 180;
    };

    var rad2deg = function (r) {
        return (r / Math.PI) * 180;
    };

    return {
        getRandomPositionInCanvas: getRandomPositionInCanvas,
        randomNumber: randomNumber,
        cutOffNumbers: cutOffNumbers,
        setCursorPosition: setCursorPosition,
        getCursorPosition: getCursorPosition,
        resizeCanvasToDisplaySize: resizeCanvasToDisplaySize,
        msToHMS: msToHMS,
        deg2rad: deg2rad,
        rad2deg: rad2deg,
    };
})();

canvas.addEventListener("mousemove", function (e) {
    tools.setCursorPosition(canvas, e);
});
