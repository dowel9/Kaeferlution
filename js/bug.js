var bugIDCounter = -1;

function getBugID() {
    bugIDCounter++;
    return bugIDCounter;
}

class Kaefer {
    constructor() {
        if (!settings.initialized) updateSettings();

        // Stats
        this.ID = getBugID();
        this.rndMovCooldown = Math.floor(tools.randomNumber(settings.rndMovement_cooldown, 5, false) * 1000);
        this.rndMovTurningTimer = tools.randomNumber(0, this.rndMovCooldown, true);
        if (settings.speed_allowed) {
            this.speed = tools.randomNumber(settings.speed_min, settings.speed_max);
        } else this.speed = (settings.speed_min + settings.speed_max) / 2;
        if (settings.size_allowed) this.size = tools.randomNumber(settings.size_min, settings.size_max);
        else this.size = (settings.size_min + settings.size_max) / 2;
        if (settings.opacity_allowed) this.opacity = tools.randomNumber(settings.opacity_min, settings.opacity_max);
        else this.opacity = 100;

        // Drawing
        this.image = new Image();
        this.image.src = "img/bug.png";
        this.drawSize = Math.max(0.75 * this.size, 10);
        this.drawAngle = null;

        // Movement and Position
        this.position = tools.getRandomPositionInCanvas(this.drawSize);
        this.hitBoxPosition = this.position;
        this.movementDir = new Vector2D(Math.random(), Math.random());
        this.movementDir.normalize();
    }

    move() {
        // If the bug turns do it now
        if (settings.rndMovement_allowed) {
            if (this.rndMovTurningTimer <= 0) this.rndMovementDirectionChange();
            else this.rndMovTurningTimer -= timeDiff;
        }

        // Normal movement: Prevent the bug from running out of the canvas and just "reflect" it when it hits the border
        let speedMult = 0.1;
        let theoreticalNewPos = new Point2D(
            this.position.x + this.speed * speedMult * this.movementDir.x,
            this.position.y + this.speed * speedMult * this.movementDir.y
        );

        let rightBound = canvas.width - 0.5 * this.drawSize;
        let leftBound = 10;
        let topBound = 10;
        let bottomBound = canvas.height - 0.5 * this.drawSize;

        if (theoreticalNewPos.x <= leftBound || theoreticalNewPos.x >= rightBound) {
            this.movementDir.x *= -1;
            theoreticalNewPos.x = this.position.x + this.speed * speedMult * this.movementDir.x;
        }
        if (theoreticalNewPos.y <= topBound || theoreticalNewPos.y >= bottomBound) {
            this.movementDir.y *= -1;
            theoreticalNewPos.y = this.position.y + this.speed * speedMult * this.movementDir.y;
        }

        this.position = theoreticalNewPos;
    }

    rndMovementDirectionChange() {
        // Reset the timer
        this.rndMovTurningTimer = this.rndMovCooldown;

        // Turn into a random direction inside the cone
        let currMovDir = this.movementDir;
        let mult = tools.randomNumber(0, 1) >= 0.5;
        if (mult) mult = 1;
        else mult = -1;

        // Random angle but at least 25% of the maxangle
        let angle = settings.rndMovement_maxDegree;
        angle = tools.randomNumber(0.25 * angle, angle);

        currMovDir.rotate(mult * angle);
        this.movementDir = currMovDir;
    }

    checkHit(point) {
        // Check for hit (we adjust the distance by 1.6 due to free space around the bug in the png that would otherwise count)
        let dist = point.distance(this.hitBoxPosition);
        if (dist < this.drawSize / 1.6) {
            return true;
        }
        return false;
    }

    draw() {
        // Get angle we have to turn our bug to
        let angle = Math.atan2(this.movementDir.y, this.movementDir.x);
        angle += Math.PI / 2;
        this.drawAngle = angle;

        // Adjust hitbox position
        var el1 = { x: this.position.x, y: this.position.y, r: 0 };
        var el2 = { x: this.position.x, y: this.position.y, angle };
        el2.parent = el1;
        var hbPos = this.getXYR(el1);
        this.hitBoxPosition = new Point2D(hbPos.x, hbPos.y);

        // Draw the bug and rotate it correctly
        context.save();
        context.globalAlpha = this.opacity / 100;
        context.translate(this.position.x, this.position.y);
        context.rotate(angle);
        context.scale(this.drawSize / 1000, this.drawSize / 1000);
        context.drawImage(this.image, -this.image.width / 2, -this.image.height / 2);

        context.restore();
    }

    drawHitBox() {
        context.save();

        context.beginPath();
        context.arc(this.hitBoxPosition.x, this.hitBoxPosition.y, 0.5 * this.drawSize, 0, 2 * Math.PI, false);
        context.lineWidth = 3;
        context.strokeStyle = "#FF0000";
        context.stroke();

        context.restore();
    }

    getXYR(node) {
        // Credits to https://stackoverflow.com/questions/8844525/get-position-on-canvas-after-rotating-translating-and-restoring
        var x, y, r, parentXYR, pX, pY, pR, nX, nY;

        x = y = r = 0;

        if (node) {
            parentXYR = this.getXYR(node.parent);
            pX = parentXYR.x;
            pY = parentXYR.y;
            pR = tools.deg2rad(parentXYR.r);
            nX = node.x;
            nY = node.y;

            x = pX + nX * Math.cos(pR) - nY * Math.sin(pR);
            y = pY + nX * Math.sin(pR) + nY * Math.cos(pR);
            r = tools.rad2deg(pR + tools.deg2rad(node.r));
        }

        return { x: x, y: y, r: r };
    }

    layEgg() {
        // The clone is the original Käfer with the exact same stats
        let clone = Object.assign(Object.create(Object.getPrototypeOf(this)), this);
        clone.ID = getBugID();

        // We now look to mutate
        let mutateChance = Math.random() * 100;
        if (mutateChance < settings.speed_mutation && settings.speed_allowed)
            clone.speed = tools.randomNumber(settings.speed_min, settings.speed_max);
        mutateChance = Math.random() * 100;
        if (mutateChance < settings.size_mutation && settings.size_allowed) {
            clone.size = tools.randomNumber(settings.size_min, settings.size_max);
            clone.drawSize = Math.max(0.75 * clone.size, 10);
        }
        mutateChance = Math.random() * 100;
        if (mutateChance < settings.rndMovement_mutation && settings.rndMovement_allowed)
            clone.rndMovCooldown = Math.floor(tools.randomNumber(settings.rndMovement_cooldown, 30, false) * 1000);

        // Chose a location and a new movementDirection
        clone.position = tools.getRandomPositionInCanvas(clone.drawSize);
        clone.movementDir = new Vector2D(Math.random(), Math.random());
        clone.movementDir.normalize();

        // We are done mutating our baby and we return it
        return clone;
    }
}
