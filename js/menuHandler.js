function mainMenu_startGame() {
    document.getElementById("mainMenuWrapper").style.display = "none";
    document.getElementById("gameWrapper").style.display = "block";
    tools.resizeCanvasToDisplaySize(canvas);
    startNewRound();
}

function mainMenu_automaticSimulation() {
    alert("This feature is still WIP");
}

function mainMenu_openSettings() {
    document.getElementById("mainMenuWrapper").style.display = "none";
    document.getElementById("settingsWrapper").style.display = "block";
}

function settingsMenu_returnToMainMenu() {
    document.getElementById("settingsWrapper").style.display = "none";
    document.getElementById("mainMenuWrapper").style.display = "block";
}

function stats_nextGen() {
    document.getElementById("statsWrapper").style.display = "none";
    document.getElementById("gameWrapper").style.display = "block";
    startTime = null;
    startNewRound();
}

function stats_returnToMainMenu() {
    statsCollector = [];
    document.getElementById("statsWrapper").style.display = "none";
    document.getElementById("mainMenuWrapper").style.display = "block";
}
