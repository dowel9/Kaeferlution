/*
    JS-file for loading and saving the settings used in the mutation of the bugs later
*/

var settings = {
    initialized: false,

    plot_fragments: 10,

    general_bugAmount: -1,
    general_time: -1,
    general_rounds: -1,
    general_pointsNeeded: -1,

    size_allowed: true,
    size_min: -1,
    size_max: -1,
    size_mutation: -1,

    speed_allowed: true,
    speed_min: -1,
    speed_max: -1,
    speed_mutation: -1,

    rndMovement_allowed: true,
    rndMovement_maxDegree: -1,
    rndMovement_cooldown: -1,
    rndMovement_mutation: -1,

    opacity_allowed: false,
    opacity_min: -1,
    opacity_max: -1,
    opacity_mutation: -1,

    // TODO: Color
    // TODO: Detect Mouse and run away from it
};

// Load our values from the settings-page into an array used in our game.js
function updateSettings() {
    settings.general_bugAmount = parseInt(document.getElementById("settings_general_bugNumber").value);
    settings.general_time = parseInt(document.getElementById("settings_general_time").value);
    settings.general_rounds = parseInt(document.getElementById("settings_general_rounds").value);
    settings.general_pointsNeeded = parseInt(document.getElementById("settings_general_points").value);

    settings.size_allowed = document.getElementById("settings_size_allow").checked;
    settings.size_min = parseInt(document.getElementById("settings_size_min").value);
    settings.size_max = parseInt(document.getElementById("settings_size_max").value);
    settings.size_mutation = parseInt(document.getElementById("settings_size_mut").value);

    settings.speed_allowed = document.getElementById("settings_speed_allow").checked;
    settings.speed_min = parseInt(document.getElementById("settings_speed_min").value);
    settings.speed_max = parseInt(document.getElementById("settings_speed_max").value);
    settings.speed_mutation = parseInt(document.getElementById("settings_speed_mut").value);

    settings.rndMovement_allowed = document.getElementById("settings_rndMov_allow").checked;
    settings.rndMovement_maxDegree = parseInt(document.getElementById("settings_rndMov_angle").value);
    settings.rndMovement_cooldown = parseInt(document.getElementById("settings_rndMov_cooldown").value);
    settings.rndMovement_mutation = parseInt(document.getElementById("settings_rndMov_mut").value);

    settings.opacity_allowed = document.getElementById("settings_opacity_allow").checked;
    settings.opacity_min = parseInt(document.getElementById("settings_opacity_min").value);
    settings.opacity_max = parseInt(document.getElementById("settings_opacity_max").value);
    settings.opacity_mutation = parseInt(document.getElementById("settings_opacity_mut").value);

    settings.initialized = true;
}

// For the settings-page to adjust the size of the bugs
var settingsPage = (function () {
    var picture_bug_size_min = document.getElementById("settings_size_min_img");
    var picture_bug_size_max = document.getElementById("settings_size_max_img");

    var size_min = function (newSize) {
        picture_bug_size_min.width = newSize.toString();
    };

    var size_max = function (newSize) {
        picture_bug_size_max.width = newSize.toString();
    };

    return {
        size_min: size_min,
        size_max: size_max,
    };
})();
