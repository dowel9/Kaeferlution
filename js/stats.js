var statsCollector = [];

function createLayout(title) {
    var statsLayout = {
        title: title,
        xaxis: { fixedrange: true },
        yaxis: { fixedrange: true },
    };
    return statsLayout;
}

function basicDataLayout(title, html, type = "bar", x = [], y = []) {
    // data structure needed by plotly
    var dataTrace = [
        {
            x: x,
            y: y,
            type: type,
        },
    ];
    var layout = createLayout(title);
    var dataStack = {
        html: html,
        layout: layout,
        data: dataTrace,
    };
    return dataStack;
}

class RoundStats {
    constructor(round, survivedBugs) {
        this.round = round;
        this.survivedBugs = survivedBugs;
        this.numberOfBugs = this.survivedBugs.length;
    }

    getAverages() {
        let averages = { size: null, speed: null, rngMovementCD: null, opacity: null };

        // Add the stats of all bugs together
        this.survivedBugs.forEach((element) => {
            if (settings.size_allowed) averages.size += element.size;
            if (settings.speed_allowed) averages.speed += element.speed;
            if (settings.rndMovement_allowed) averages.rngMovementCD += element.rndMovCooldown;
            if (settings.opacity_allowed) averages.opacity += element.opacity;
        });

        // Divide the sum by the number of bugs
        for (const [key, value] of Object.entries(averages)) {
            if (value != null) averages[key] = value / this.numberOfBugs;
        }

        return averages;
    }

    _basicDataLayout(titel, html, type = "bar", x = [], y = []) {
        // data structure needed by plotly
        var dataTrace = [
            {
                x: x,
                y: y,
                type: type,
            },
        ];
        var layout = createLayout(titel);
        var dataStack = {
            html: html,
            layout: layout,
            data: dataTrace,
        };
        return dataStack;
    }

    _formatDataX(title, html, settMin, settMax, keyName) {
        // Prevent the ability to zoom in and also set a title
        let dataStack = this._basicDataLayout(title, html);
        // Needed Variables for stats
        let fragmentSize = (settMax - settMin) / settings.plot_fragments;

        // Bugs have small differenzes. We create smaller groups of them (all bugs with speed 0-10 in one group f.e.)
        for (let i = 0; i < settings.plot_fragments; i++) {
            let fragmentStart = settMin + i * fragmentSize;
            fragmentStart = Math.floor(fragmentStart * 100) / 100;
            let fragmentStop = settMin + (i + 1) * fragmentSize;
            fragmentStop = Math.floor(fragmentStop * 100) / 100;

            // Push it into the array to be displayed
            dataStack.data[0].x.push(fragmentStart + " - " + fragmentStop);
            dataStack.data[0].y[i] = 0;

            // Sort in the bugs
            this.survivedBugs.forEach((element) => {
                if (element[keyName] >= fragmentStart && element[keyName] < fragmentStop) dataStack.data[0].y[i] += 1;
            });
        }

        return dataStack;
    }

    formatDataSpeed() {
        let speedMin = settings.speed_min;
        let speedMax = settings.speed_max;
        let dataStack = this._formatDataX(
            "Langsam <- Geschwindigkeit der Käfer -> Schnell",
            "statsDivSpeed",
            speedMin,
            speedMax,
            "speed"
        );

        return dataStack;
    }

    formatDataSize() {
        let sizeMin = settings.speed_min;
        let sizeMax = settings.speed_max;
        let dataStack = this._formatDataX("Klein <- Größe der Käfer -> Groß", "statsDivSize", sizeMin, sizeMax, "size");

        return dataStack;
    }

    formatDataRngMovement() {
        let rngMovMin = settings.rndMovement_cooldown * 1000;
        let rngMovMax = 8000;
        let dataStack = this._formatDataX(
            "Häufig <- Zufällige Richtungsänderungen der Käfer alle X Millisekunden -> Selten",
            "statsDivRnGMovement",
            rngMovMin,
            rngMovMax,
            "rndMovCooldown"
        );

        return dataStack;
    }

    formatDataOpacity() {
        let opacityMin = settings.opacity_min;
        let opacityMax = settings.opacity_max;
        let dataStack = this._formatDataX(
            "Durchsichtbar <- Durchsichtbarkeit der Käfer -> Gut sichtbar",
            "statsDivOpacity",
            opacityMin,
            opacityMax,
            "opacity"
        );

        return dataStack;
    }
}
